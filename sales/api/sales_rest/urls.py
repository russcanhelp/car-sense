from django.urls import path
from .views import list_salespeople, show_salesperson_detail, list_customers, show_customer_detail, list_sales, show_sale_detail, api_list_automobiles

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:id>/", show_salesperson_detail, name="show_salesperson_detail"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:id>/", show_customer_detail, name="show_customer_detail"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:id>/", show_sale_detail, name="show_sale_detail"),
    path("automobiles/", api_list_automobiles, name="api_list_automobiles"),
]
