from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
from django.http import JsonResponse
import json
from common.json import ModelEncoder

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin', 'sold']


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ['first_name', 'last_name', 'address', 'phone_number', 'id']

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [ 'id', 'first_name', 'last_name', 'employee_id']

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [ 'id', 'price', 'automobile','salesperson', 'customer' ]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople  = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder = SalespersonEncoder
        )
    else:
        content = json.loads(request.body)
        salespeople = Salesperson.objects.create(**content)
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
            safe=False
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def show_salesperson_detail(request, id):
    if request.method == "GET":
        salespeople = Salesperson.objects.get(id=id)
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(id=id).update(**content)
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False
        )

@require_http_methods(["GET","DELETE","PUT"])
def show_customer_detail(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {'sales': sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            salesperson_id = content["salesperson"]
            customer_id = content["customer"]
            automobile_vin = content["automobile"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            customer = Customer.objects.get(id=customer_id)
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["salesperson"] = salesperson
            content["customer"] = customer
            content["automobile"] = automobile
            automobile.sold = True
        except (Salesperson.DoesNotExist, Customer.DoesNotExist, AutomobileVO.DoesNotExist):
            return JsonResponse(
                {"message": "Invalid input"},
                status=400
            )

        sale = Sale.objects.create(**content)

        return JsonResponse(
            {"sale": sale},
            encoder=SaleEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def show_sale_detail(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            {"sale": sale},
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Sale.objects.filter(id=id).update(**content)
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            {"sale": sale},
            encoder=SaleEncoder,
            safe=False,
        )

@require_http_methods("GET")
def api_list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
        )
