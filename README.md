# CarCar
CarCar is a web application for a car dealership and service center. The application allows them to manage their inventory, sales and service appointments.

Team:

* Russell Cruz - Sales Microservice
* Sam Siskind - Service Microservice

## Getting started with CarCar

### Prequisites

You'll need to have installed [Docker](https://www.docker.com/get-started/), [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git), and [Node.js](https://nodejs.org/en) in order to run the application.

### Instructions

1. Visit the [project repo](https://gitlab.com/swamswiskind/project-beta)
2. Fork the repository to your own account.
3. Copy the clone url to your clipboard.
4. Within terminal, navigate to the directory that you want to clone the repo into.
5. In your terminal Clone the forked repository onto your computer using:

```
git clone <<your.repository.url.here>>
```
6. Within terminal cd into your new directory created by the clone command.
7. Open Docker Desktop on your computer and wait for it be running.
8. Build and run the project with Docker using the following commands in order.

```
docker volume create beta-data
docker-compose build
docker-compose up
```

9. After running these commands, make sure all of your Docker containers are running.
10. You can view the front-end of the application by visiting [http://localhost:3000/](http://localhost:3000/) in your browser.
11. If you use Insomnia, you can import [this collection](CarCarEndpoints.json) into your Insomnia to be able to test the endpoints easily.

## Design

### The microservices:

- **Inventory Microservice:** Keeps track of the automobile inventory for the automobile dealership.
- **Service Microservice:** Keeps track of the service appointment history for the automobile dealership's service center.
- **Sales Microservice:** Keeps track of the sales history for the automobile dealership.

#### Diagram

![Russell and Sam's Project beta diagram!](Project-Beta-Diagram.png "Russell and Sam's Project beta diagram")

#### How they work together

The microservices in this application communicate using polling.

Both the service and sales microservices poll the inventory microservice to create Automobile Value Objects. The Automobile Value Object was created to pass along the ID, VIN and sold status of any automobile in the dealership's inventory.

The service microservice also polls the sales microservice to create a Customer Value Object. The Customer Value Object was created to allow a person scheduling an appointment to select a customer created with the Sales API's Create Customer endpoint. This was done to limit the possibility of duplicate customer records based on typos and changes in capitalization. Customer records can be created on the Sales API without needing the customer to be associated with an existing Sale so that service appointments can be scheduled for automobiles not purchased from the dealership.

The appointment list and service history list components from the react front-end use the Sales API to determine if an appointment vin corresponds to a vin on an existing sale.

## Inventory microservice

The application's Inventory API can keep track of the automobile inventory for the automobile dealership.

It has RESTful endpoints for the following entities:

- **Manufacturer:** The company that manufactures the automobile.
- **Vehicle Model:** The model of a vehicle created by the manufacturer.
- **Automobile:** The actual automobile of a specific vehicle model.

The following documentation describes the available functionality in the Inventory API.

### Manufacturers:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

#### Create and Update a manufacturer:

Creating and updating a manufacturer requires only the manufacturer's name. Manufacturers should be given a unique name

```
{
  "name": "Daimler-Chrysler"
}
```

The return value of creating, getting, and updating a single manufacturer is its name, href, and id.

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Daimler-Chrysler"
}
```
#### Get a list of manufacturers:

The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers.

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    },
    {
      "href": "/api/manufacturers/2/",
      "id": 2,
      "name": "Volkswagen"
    }

  ]
}
```

### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

#### Create and update a vehicle model:

Creating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer. The picture URL should be a properly formatted URL as the model expects a URL, not string.


```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or the picture URL. It is not possible to update a vehicle model's manufacturer. You would need to delete the model and create a new one to have the model associated with a different manufacturer.


```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```

Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information and the manufacturer's information. The manufacturer is a nested object in these responses.

```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```

Getting a list of vehicle models returns a list of the detail information with the key "models".


```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    },
    {
      "href": "/api/models/2/",
      "id": 2,
      "name": "Jetta",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/d/d7/2019_Volkswagen_Jetta_front_7.11.18.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/2/",
        "id": 2,
        "name": "Volkswagen"
      }
    }
  ]
}
```

### Automobiles:

> Note: The **'vinnumber'** at the end of the detail GET, PUT, and DELETE endpoints represents the VIN number for the specific automobile that you want to interact with. This is a string value since VIN numbers are alphanumeric.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vinnumber/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vinnumber/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vinnumber/

#### Create an automobile:

You can create an automobile with its color, year, VIN, and the id of the vehicle model.


```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```

#### Return Value of Creating an Automobile:

```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```

As noted, you query an automobile by its VIN. For example, you would use the URL
[http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/](http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/) to get the details for the car with the VIN "1C3CC5FB2AN120174". The details for an automobile include its model and manufacturer.

#### Return Value of getting an automobile:

```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```

You can update the color, year, and sold status of an automobile.

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```

#### Get list of Automobiles:

Getting a list of automobiles returns a dictionary with the key "autos" set to a list of automobile information.

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    },
    {
      "href": "/api/automobiles/F495859384904949/",
      "id": 2,
      "color": "red",
      "year": 2013,
      "vin": "F495859384904949",
      "model": {
        "href": "/api/models/2/",
        "id": 2,
        "name": "Jetta",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/d/d7/2019_Volkswagen_Jetta_front_7.11.18.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/2/",
          "id": 2,
          "name": "Volkswagen"
        }
      },
      "sold": true
    }

  ]
}
```

## Service microservice

The application's Service API keeps track of the service appointment history for the automobile dealership's service center.

It has RESTful endpoints for the following entities:

- **Technician:** The model of a technician that works at the service center and can be assigned to a service appointment.
- **Appointment:** The model of a service appointment for a vehicle, that is associated with a technician, VIN and customer.

>Note: The Customer VO object was created to allow a person making an appointment to select a customer created with the Sales API Create Customer endpoint. This was done to limit the possibility of duplicate customer records based on typos and changes in capitalization.


> Note: The VIN associated with an appointment can be for a vehicle purchased through the dealership or outside the dealership. If the VIN matches a VIN associated with a sale on the Sales API, the customer will be marked as a VIP on the Appointment list component and Service History list component.


The following documentation describes the available functionality in the Service API.

### Technicians:

> Note: The **'id'** at the end of the DELETE endpoint for a specific technician represents the id value for the specific technican that you want to interact with. This is a number since id's for DB rows are numeric.

Technicians can not be updated, you would need to delete a technican and create a new one. Additionally, Technicians that have ever been assigned to an appointment cannot be deleted. This is to preserve the record of appointments in the service history component.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technician | GET | http://localhost:8080/api/technicians/ |
| Create a technician | POST | http://localhost:8080/api/technicians/ |
| Delete a specific technician | DELETE | http://localhost:8080/api/technicans/id/ |

#### Create a technician:

Send a JSON object that contains a technician's first name, last name and employee id to [http://localhost:8080/api/technicians/](http://localhost:8080/api/technicians/)

> Note: employee_id values must be unique for each technician.

```
{
	"first_name": "Sam",
	"last_name": "Siskind",
	"employee_id": "ssiskind"
}
```

The response will be the JSON object for the technician you just created. This will include their ID.

```
{
	"employee_id": "ssiskind",
	"first_name": "Sam",
	"last_name": "Siskind",
	"id": 1
}
```


 > Note: Using another technician's employee ID will return **400** response with the message:

```
{
	"message": "duplicate key value violates unique constraint \"service_rest_technician_employee_id_5f205d09_uniq\"\nDETAIL:  Key (employee_id)=(ksiskind) already exists.\n"
}
```

#### Get List of technicians

Getting a list of technicians returns a dictionary with the key "technicians" set to a list of each technician's employee id, first name, last name, and DB ID number.

```
{
	"technicians": [
		{
			"employee_id": "ksiskind",
			"first_name": "Keiko",
			"last_name": "Siskind",
			"id": 2
		},
		{
			"employee_id": "ssiskind",
			"first_name": "Sam",
			"last_name": "Siskind",
			"id": 1
		}
	]
}
```

#### Delete a technician

To delete a technician you'll use the DELETE method on the url [http://localhost:8080/api/technicians/id/](http://localhost:8080/api/technicians/id/) where the id value is replaced with the id number of the technician you are deleting.

A successful response will return a **200** status and the below message.

```
{
	"message": "The technician was deleted"
}
```

An unsuccessful response will return a **400** status and the below message.

```
{
	"message": "Technician does not exist"
}
```

### Appointments:

> Note: The **'id'** at the end of the GET, PUT endpoints for a specific appointment represents the id value for the specific appointment that you want to interact with. This is a number since id's for DB rows are numeric.

Appointments should not be deleted as that would interfere with the record of appointments in the service history component.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Appointments | GET | http://localhost:8080/api/appointments/ |
| Create an Appointment | POST | http://localhost:8080/api/appointments/ |
| Update an Appointment | PUT | http://localhost:8080/api/appointments/id/ |
| Finish an Appointment | PUT | http://localhost:8080/api/appointments/id/finish/ |
| Cancel an Appointment | PUT | http://localhost:8080/api/appointments/id/cancel/ |

#### List Appointments

Getting a list of appointments returns a dictionary with the key "appointments" set to a list of each appointments's customer, date and time, vin, reason, status,technician, and DB ID number.

```
{
	"appointments": [
		{
			"href": "/api/appointments/1/",
			"id": 1,
			"vin": "PWBFF3839R984",
			"date_time": "2024-07-03T09:00:00+00:00",
			"reason": "They want their tire changed.",
			"status": {
				"id": 1,
				"name": "SCHEDULED"
			},
			"technician": {
				"employee_id": "ssiskind",
				"first_name": "Sam",
				"last_name": "Siskind",
				"id": 1
			},
			"customer": {
				"customer_vo_id": 1,
				"first_name": "Tingyu",
				"last_name": "Chen",
				"phone_number": "555-555-6655"
			}
		},
		{
			"href": "/api/appointments/2/",
			"id": 2,
			"vin": "7896055",
			"date_time": "2024-07-03T09:00:00+00:00",
			"reason": "They want their oil changed.",
			"status": {
				"id": 1,
				"name": "SCHEDULED"
			},
			"technician": {
				"employee_id": "ksiskind",
				"first_name": "Keiko",
				"last_name": "Siskind",
				"id": 1
			},
			"customer": {
				"customer_vo_id": 1,
				"first_name": "Tingyu",
				"last_name": "Chen",
				"phone_number": "555-555-6655"
			}
		}
	]
}
```

#### Create an Appointment

> date_time field must receive a string value that is formatted in the same way as the below example. Additionally, customer, technician and status are all expecting the numeric id as their values.

```
{
	"vin": "7896055",
	"customer": 1,
	"date_time": "2024-07-03T09:00:00Z",
	"technician": 1,
	"reason": "They want their tire changed.",
	"status": 1

}
```

A successful response will return a **200** status and the below object.

```
{
	"href": "/api/appointments/2/",
	"id": 2,
	"vin": "7896055",
	"date_time": "2024-07-03T09:00:00Z",
	"reason": "They want their tire changed.",
	"status": {
		"id": 1,
		"name": "SCHEDULED"
	},
	"technician": {
		"employee_id": "ssiskind",
		"first_name": "Sam",
		"last_name": "Siskind",
		"id": 1
	},
	"customer": {
		"customer_vo_id": 1,
		"first_name": "Tingyu",
		"last_name": "Chen",
		"phone_number": "555-555-6655"
	}
}
```

An unsuccessful response will return a variety of granular **400** and **404** status codes matching the error handling for this endpoint.

```
        except CustomerVO.DoesNotExist:
            response = JsonResponse(
                {"message": "The provided customer does not exist."}
            )
            response.status_code = 404
            return response
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "The provided technician does not exist."}
            )
            response.status_code = 404
            return response
        except Status.DoesNotExist:
            response = JsonResponse({"message": "The provided status does not exist."})
            response.status_code = 404
            return response
        except KeyError as error:
            response = JsonResponse(
                {"message": f"The key named {error.args[0]} not found in request body."}
            )
            response.status_code = 400
            return response
```

#### Update an Appointment

Updating an appointment requires the same object as creating an appointment, but the url ([http://localhost:8080/api/appointments/id/](http://localhost:8080/api/appointments/id/)) that you use in your PUT request, must contain the appointment's DB id.

```
{
	"vin": "PWBFF38394984",
	"customer": 1,
	"date_time": "2022-10-31T09:00:00Z",
	"technician": 1,
	"reason": "They want their oil changed.",
	"status": 1
}
```

A successfull response will return the details of the updated appointment

```
{
	"href": "/api/appointments/1/",
	"id": 1,
	"vin": "PWBFF38394984",
	"date_time": "2022-10-31T09:00:00Z",
	"reason": "They want their oil changed.",
	"status": {
		"id": 1,
		"name": "SCHEDULED"
	},
	"technician": {
		"employee_id": "ssiskind",
		"first_name": "Sam",
		"last_name": "Siskind",
		"id": 1
	},
	"customer": {
		"customer_vo_id": 1,
		"first_name": "Tingyu",
		"last_name": "Chen",
		"phone_number": "555-555-6655"
	}
}
```

An unsucessful response will return **400** and **404** errors matching similar error handling logic as seen above in the create an appointment section. You can update the status of an appointment using this endpoint, but below you'll find are easier ways to update the status of an appointment.

#### Finish an Appointment

Finishing an appointment, does not take a request body. The url ([http://localhost:8080/api/appointments/id/finish/](http://localhost:8080/api/appointments/id/finish/)) that you use in your PUT request, must contain the appointment's DB id and will automatically change the status of the appointment to FINISHED.

A sample response would be:

```
{
	"href": "/api/appointments/9/",
	"id": 9,
	"vin": "4857FUBBJFBJ4",
	"date_time": "2022-10-31T09:00:00+00:00",
	"reason": "They want their oil changed.",
	"status": {
		"id": 2,
		"name": "FINISHED"
	},
	"technician": {
		"employee_id": "ksiskind",
		"first_name": "Keiko",
		"last_name": "Siskind"
	},
	"customer": {
		"customer_vo_id": 0,
		"first_name": "Sam",
		"last_name": "Siskind",
		"phone_number": "555-455-4323"
	}
}
```
If you attempt to update the status of an appointment that does not exist, you'll get a **400** status error response of:

```
{
	"message": "Appointment does not exist"
}
```


#### Cancel an Appointment

Canceling an appointment, does not take a request body. The url ([http://localhost:8080/api/appointments/id/cancel/](http://localhost:8080/api/appointments/id/cancel/)) that you use in your PUT request, must contain the appointment's DB id and will automatically change the status of the appointment to CANCELED.

A sample response would be:

```
{
	"href": "/api/appointments/9/",
	"id": 9,
	"vin": "4857FUBBJFBJ4",
	"date_time": "2022-10-31T09:00:00+00:00",
	"reason": "They want their oil changed.",
	"status": {
		"id": 2,
		"name": "CANCELED"
	},
	"technician": {
		"employee_id": "ksiskind",
		"first_name": "Keiko",
		"last_name": "Siskind"
	},
	"customer": {
		"customer_vo_id": 0,
		"first_name": "Sam",
		"last_name": "Siskind",
		"phone_number": "555-455-4323"
	}
}
```
If you attempt to update the status of an appointment that does not exist, you'll get a **400** status error response of:

```
{
	"message": "Appointment does not exist"
}
```

## Sales microservice
***

The application's Sales API can keep track of the Sales for the automobile dealership.
It has RESTful endpoints for the following entities:
- Salesperson: the people who are employed at the dealership that sell cars
- Customer: the people who bought or may potentially buy a vehicle
- Sale: keeps track of sales for the dealership

### Models
1. Salesperson
2. Customer
3. Sale
4. Automobile VO

---
#### Salesperson Model
- Salesperson INFO

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | 	http://localhost:8090/api/salespeople/
| Create a salesperson | POST | 	http://localhost:8090/api/salespeople/
| Show a specific salesperson | GET | 	http://localhost:8090/api/salespeople/id/
| Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/id/
| Update a specific salesperson | PUT | http://localhost:8090/api/salespeople/id/

**[GET]** List all salespeople Return Value (PREVIEW):
```
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Russell",
            "last_name": "Cruz",
			"employee_id": "rCruz1"
		}
	]
}
```

**[POST]** Create a Salesperson (INSIDE JSON BODY):
```
{
	"first_name": "Russell",
    "last_name": "Cruz",
	"employee_id": "rCruz1"
}
```
Return value of creating salesperson (PREVIEW):
```
{
	"salespeople": {
		"id": 1,
		"first_name": "Russell",
		"last_name": "Cruz",
		"employee_id": "rCruz1"
	}
}
```

**[GET]** Show a specific salesperson:
- *To show a specific salesperson. Replace "ID" in URL with the salesperson's "id"*
```
	http://localhost:8090/api/salespeople/ID/
```
Return value when getting a specific salesperson(PREVIEW)
```
{
	"salespeople": {
			"id": 1,
			"first_name": "Russell",
            "last_name": "Cruz",
			"employee_id": "rCruz1"
	}

}
```

**[DELETE]** Delete specific salesperson:
- *To delete a specific salesperson. Replace "ID" in URL with the salesperson's "id"*
```
	http://localhost:8090/api/salespeople/ID/
```

Return value of delete salesperson (PREVIEW):
```
{
	"deleted": true
}
```

**[PUT]** Update a Salesperson:

- *To update a specific salesperson. Replace "ID" in URL with the salesperson's "id"*
```
	http://localhost:8090/api/salespeople/ID/
```
Make updates (INSIDE JSON BODY)
```
{
	"first_name": "Russell Duane",
    "last_name": "Cruz",
	"employee_id": "rDuaneCruz1"
}
```
Return value of update salesperson (PREVIEW):
```
{
	"id": 1,
	"first_name": "Russell",
    "last_name": "Cruz",
	"employee_id": "rDuaneCruz1"
}
```
---
#### Customer Model

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | 	http://localhost:8090/api/customers/
| Create a customer | POST | 	http://localhost:8090/api/customers/
| Show a specific customer | GET | 	http://localhost:8090/api/customers/id/
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/id/
| Update a specific customer | PUT | http://localhost:8090/api/customers/id/

**[GET]** List all customers Return Value (PREVIEW):
```
{
	"customers": [
		{
			"first_name": "John",
			"last_name": "Doe",
			"address": "123 Anywhere Rd, Someplace, CA, 90001",
			"phone_number": "777-888-9999",
			"id": 1
		}
	]
}
```

**[POST]** Create a customer (INSIDE JSON BODY):
```
{
	"first_name": "John",
	"last_name": "Doe",
	"address": "123 Anywhere Rd, Someplace, CA, 90001",
	"phone_number": "777-888-9999"
}
```
Return value of creating a customer (PREVIEW):
```
{
	"customer": {
		"first_name": "John",
		"last_name": "Doe",
		"address": "123 Anywhere Rd, Someplace, CA, 90001",
		"phone_number": "777-888-9999",
		"id": 1
	}
}
```

**[GET]** Show a specific customer:
- *To show a specific customer. Replace "ID" in URL with the customers's "id"*
```
	http://localhost:8090/api/customers/ID/
```
Return value when getting a specific customer (PREVIEW)
```
{
	"customer": {
		"first_name": "John",
		"last_name": "Doe",
		"address": "123 Anywhere Rd, Someplace, CA, 90001",
		"phone_number": "777-888-9999",
		"id": 1
	}
}
```

**[DELETE]** Delete specific customer:
- *To delete a specific customer. Replace "ID" in URL with the customer's "id"*
```
	http://localhost:8090/api/customers/ID/
```

Return value of deleting a customer (PREVIEW):
```
{
	"deleted": true
}
```

**[PUT]** Update a customer:

- *To update a specific customer. Replace "ID" in URL with the customer's "id"*
```
	http://localhost:8090/api/customers/ID/
```
Make updates (INSIDE JSON BODY)
```
{
	"first_name": "John",
	"last_name": "Snow",
	"address": "123 Anywhere Rd, Someplace, CA, 90001",
	"phone_number": "777-888-9999"
}
```
Return value of updating a customer (PREVIEW):
```
{
	"first_name": "John",
	"last_name": "Snow",
	"address": "123 Anywhere Rd, Someplace, CA, 90001",
	"phone_number": "777-888-9999",
    "id": 1
}
```
---
#### Sale Model

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List sales | GET | 	http://localhost:8090/api/sales/
| Create a sale | POST | 	http://localhost:8090/api/sales/
| Show a specific sale | GET | 	http://localhost:8090/api/sales/id/
| Delete a specific sale | DELETE | http://localhost:8090/api/sales/id/
| Update a specific sale | PUT | http://localhost:8090/api/sales/id/

**[GET]** List all sales Return Value (PREVIEW):
```
{
	"sales": [
		{
			"id": 1,
			"price": 40000,
			"automobile": {
				"vin": "123abc",
				"sold": true
			},
			"salesperson": {
				"id": 1,
				"first_name": "Russell",
				"last_name": "Cruz",
				"employee_id": "rCruz1"
			},
			"customer": {
				"first_name": "John",
				"last_name": "Doe",
				"address": "123 Anywhere Rd, Someplace, CA, 90001",
				"phone_number": "777-888-9999",
				"id": 1
			}
		}
	]
}
```

**[POST]** Create a sale (INSIDE JSON BODY):
- Instructions:
1. For automobile you would input a VIN available in inventory
(*This could take up 1 minute after creating a sale*)

2. For salesperson you would input the salesperson's "id"
3. For customer you would input the customer's "id"

```
{
	"automobile": "123abc",
	"salesperson": 1,
	"customer": 1,
	"price": 40000
}
```
Return value of creating a sale (PREVIEW):
```
{
	"sale": {
		"id": 1,
		"price": 40000,
		"automobile": {
			"vin": "123abc",
			"sold": true
		},
		"salesperson": {
			"id": 1,
			"first_name": "Russell",
			"last_name": "Cruz",
			"employee_id": "rCruz1"
		},
		"customer": {
			"first_name": "John",
			"last_name": "Doe",
			"address": "123 Anywhere Rd, Someplace, CA, 90001",
			"phone_number": "777-888-9999",
			"id": 1
		}
	}
}
```

**[GET]** Show a specific sale:
- *To show a specific sale. Replace "ID" in URL with the sale "id"*
```
	http://localhost:8090/api/sales/ID/
```
Return value when getting a specific sale (PREVIEW)
```
{
	"sale": {
		"id": 1,
		"price": 40000,
		"automobile": {
			"vin": "123abc",
			"sold": true
		},
		"salesperson": {
			"id": 1,
			"first_name": "Russell",
			"last_name": "Cruz",
			"employee_id": "rCruz1"
		},
		"customer": {
			"first_name": "John",
			"last_name": "Doe",
			"address": "123 Anywhere Rd, Someplace, CA, 90001",
			"phone_number": "777-888-9999",
			"id": 1
		}
	}
}
```

**[DELETE]** Delete specific sale:
- *To delete a specific sale. Replace "ID" in URL with the sale "id"*
```
	http://localhost:8090/api/sales/ID/
```

Return value of deleting a sale (PREVIEW):
```
{
	"deleted": true
}
```

**[PUT]** Update a sale:

- *To update a specific sale. Replace "ID" in URL with the sale "id"*
```
	http://localhost:8090/api/sales/ID/
```
Make updates (INSIDE JSON BODY)
```
{
	"salesperson": 2,
	"customer": 2,
	"price": 35000
}
```
Return value of updating a sale (PREVIEW):
```
{
	"sale": {
		"id": 1,
		"price": 35000,
		"automobile": {
			"vin": "123abc",
			"sold": true
		},
		"salesperson": {
			"id": 2,
			"first_name": "Russell",
			"last_name": "Cruz",
			"employee_id": "rCruz2"
		},
		"customer": {
			"first_name": "John",
			"last_name": "Doe2",
			"address": "123 Anywhere Rd, Someplace, CA, 90001",
			"phone_number": "777-888-9999",
			"id": 2
		}
	}
}
```
---
#### AutomobileVO
- This model allows us to poll data every minute from the Automobile model in the Inventory API to continuously update it's data. If a vehicle is created it may not show up on the page until up to a minute
