import React, { useState } from 'react';

function ModelForm( props ) {

    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [picture_url, setPictureURL] = useState('');

    async function handleSubmit(event) {
    event.preventDefault();

    const data = {}
    data.name = name;
    data.manufacturer_id = manufacturer;
    data.picture_url = picture_url;

    const url = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        props.getModelList();
        props.setShowToast(true);
        props.setToastVariant('success');
        props.setToastMessage(`Added ${name} to inventory.`)
        setName('');
        setManufacturer('');
        setPictureURL('');
    } else {
        console.error(`Error adding ${name} to inventory. Please check that your picture url is a properly formatted URL`)
        props.setShowToast(true);
        props.setToastVariant('danger');
        props.setToastMessage(`Error adding ${name} to inventory. Please check that your picture url is a properly formatted URL`)
    }
  };

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleChangePicture = (event) => {
        const value = event.target.value;
        setPictureURL(value);
    }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new model</h1>
          <form onSubmit={handleSubmit} id="create-model-form">

            <div className="form-floating mb-3">
              <input onChange={handleChangeName} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangePicture} value={picture_url} placeholder="Picture URL" required type="text" name="pictureurl" id="pictureurl" className="form-control" />
              <label htmlFor="pictureurl">Picture URL</label>
            </div>

            <div className="mb-3">
              <select onChange={handleChangeManufacturer} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                <option value="">Choose a manufacturer</option>
                {props.manufacturerList.map(manufacturer => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
        {props.toast}
      </div>
    </div>
  )
}

export default ModelForm;
