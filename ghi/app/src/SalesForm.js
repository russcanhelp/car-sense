import React, { useState, useEffect } from 'react'

function SalesForm({ getSalesList, salesList, getAutomobileList, automobileList, getSalespersonList, salespersonList, getCustomersList, customersList, toast, showToast, setShowToast, toastMessage, setToastMessage, toastVariant, setToastVariant }) {

    const [automobile, setAutomobile] = useState('')
    const [salesperson, setSalesperson] = useState('')
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')

    useEffect(() => {
        getAutomobileList();
        getCustomersList();
        getSalespersonList();
        // eslint-disable-next-line
      },[])

    const handleAutomobile = (event) => {
        const value = event.target.value
        setAutomobile(value)
    }

    const handleSalesperson = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    const handleCustomer = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handlePrice = (event) => {
        const value = event.target.value
        setPrice(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.automobile = automobile
        data.salesperson = salesperson
        data.customer = customer
        data.price = price

        const salesUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {
            getSalesList()
            getAutomobileList()

            setShowToast(true);
            setToastVariant('success');
            setToastMessage(`Sale for $${price} created.`)

            setAutomobile('')
            setSalesperson('')
            setCustomer('')
            setPrice('')
        } else {
            setShowToast(true);
            setToastVariant('danger');
            setToastMessage("Error creating sale.")
        }
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Record a new sale</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">


              <div className="mb-3">
                <p>Automobile Vin</p>
                <select onChange={handleAutomobile} value={automobile} required name="automobile" id="automobile" className="form-select">
                    <option>Choose an automobile VIN...</option>
                    {automobileList.map( automobile => {
                        const isAvailable = !salesList.some(sale => sale.automobile.vin === automobile.vin);
                        if (isAvailable) {
                            return (
                                <option key={automobile.vin} value={automobile.vin}>
                                    {automobile.vin}
                                </option>
                            );
                        } else {
                            return null; // Return null when isAvailable is not true.
                        }
                    })}
                </select>
            </div>

                <div className="mb-3">
                    <p>Salesperson</p>
                    <select onChange={handleSalesperson} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                        <option>Choose a salesperson...</option>
                        {salespersonList.map( salesperson => {
                            return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name} | Employee ID: {salesperson.employee_id}
                                </option>
                                );
                            })}
                    </select>
                </div>

                <div className="mb-3">
                    <p>Customer</p>
                    <select onChange={handleCustomer} value={customer} required name="customer" id="customer" className="form-select">
                        <option>Choose a customer...</option>
                        {customersList.map( customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.first_name} {customer.last_name}
                                </option>
                                );
                            })}
                    </select>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handlePrice} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control"/>
                    <label htmlFor="price">Price</label>
                </div>

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
            {toast}
          </div>
        </div>
      )
}
export default SalesForm
