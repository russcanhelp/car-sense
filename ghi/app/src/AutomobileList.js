import React from "react";
function AutomobileList(props) {
  async function deleteAutomobile(automobileVin=null) {
    const url = `http://localhost:8100/api/automobiles/${automobileVin}/`;
    const fetchConfig = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      props.getAutomobilesList();
      props.setShowToast(true);
      props.setToastVariant('success');
      props.setToastMessage(`Automobile with the vin ${automobileVin} deleted.`)
    } else {
      console.error(`Error deleting automobile with the vin ${automobileVin}`);
      props.setShowToast(true);
      props.setToastVariant('danger');
      props.setToastMessage(`Error deleting automobile with the vin ${automobileVin}`)
    }

  }

  let tableRows;
        if (props.automobilesList.length > 0 ) {
          tableRows = props.automobilesList.map(automobile => {
            const isSold = props.salesList.some(sale => sale.automobile.vin === automobile.vin)
            return (
              <tr key={automobile.vin} >
                  <td>{automobile.vin}</td>
                  <td>{automobile.color}</td>
                  <td>{automobile.year}</td>
                  <td>{automobile.model.name }</td>
                  <td><img width={100} src={ automobile.model.picture_url } alt={automobile.model.name}></img></td>
                  <td>{automobile.model.manufacturer.name }</td>
                  <td>{ isSold  ? 'Yes' : 'No'}</td>
                  <td>
                  <button type="button" className="btn btn-outline-danger" onClick={() => deleteAutomobile(automobile.vin)}>
                  Delete
                  </button>
                </td>
              </tr>
            )
          })
        } else {
          tableRows =
          <tr>
            <td colSpan="8">No automobiles created yet.</td>
          </tr>
        }

  return (
    <div>
      <h1>Automobile List</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Model Picture</th>
          <th>Manufacturer</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {tableRows}
      </tbody>
    </table>
    {props.toast}
    </div>
  )
}

export default AutomobileList;
