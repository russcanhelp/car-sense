import React, { useState } from 'react';

function TechnicianForm( props ) {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');


    async function handleSubmit(event) {
    event.preventDefault();

    const data = {}
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeId;

    const url = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        props.getTechnicianList();
        props.setShowToast(true);
        props.setToastVariant('success');
        props.setToastMessage("Technician created.")

        setFirstName('');
        setLastName('');
        setEmployeeId('');
    } else {
      console.error("Error creating technician.");
      props.setShowToast(true);
      props.setToastVariant('danger');
      props.setToastMessage(`A technician with the employee ID ${employeeId} already exists.`)
    }
  };

    const handleChangeEmployeeId = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleChangeFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleChangeLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new technican</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">

            <div className="form-floating mb-3">
              <input onChange={handleChangeFirstName} value={firstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
              <label htmlFor="first_name">First Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangeLastName} value={lastName} placeholder="Last Name URL" required type="text" name="last_name" id="last_name" className="form-control" />
              <label htmlFor="last_name">Last Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangeEmployeeId} value={employeeId} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
              <label htmlFor="employee_id">Employee ID</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
        {props.toast}
      </div>
    </div>
  )
}

export default TechnicianForm;
