import React from "react";
function ModelList(props) {

  async function deleteModel(modelId=null, modelName) {
    const url = `http://localhost:8100/api/models/${modelId}/`;
    const fetchConfig = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      props.getModelList()
      props.setShowToast(true);
      props.setToastVariant('success');
      props.setToastMessage(`Vehicle model ${modelName} deleted.`)
    } else {
      console.error(`Error deleting vehicle model ${modelName}.`);
      props.setShowToast(true);
      props.setToastVariant('danger');
      props.setToastMessage(`Error deleting vehicle model ${modelName}.`)
    }

  }

  let tableRows;
  if (props.modelList.length > 0 ) {
    tableRows = props.modelList.map(model => {
      return (
        <tr key={model.id} >
          <td>{ model.name }</td>
          <td>{ model.manufacturer.name }</td>
          <td><img width={100} src={ model.picture_url } alt={model.name}></img></td>
          <td>
            <button type="button" className="btn btn-outline-danger" onClick={() => deleteModel(model.id, model.name)}>
            Delete
            </button>
          </td>
        </tr>
      )
    })
  } else {
    tableRows =
    <tr>
    <td colSpan="4">No vehicle models created yet.</td>
    </tr>
  }

  return (
    <div>
      <h1>Vehicle Model List</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {tableRows}
      </tbody>
    </table>
    {props.toast}
    </div>
  )
}

export default ModelList;
