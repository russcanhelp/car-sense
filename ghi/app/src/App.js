import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Toast, ToastContainer} from 'react-bootstrap';
import MainPage from './MainPage';
import Nav from './Nav';
import ModelList from './ModelList';
import AutomobileList from './AutomobileList';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileForm from './AutomobileForm';
import ModelForm from './ModelForm';
import AppointmentList from './AppointmentList';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import CustomerForm from './CustomerForm';
import ServiceHistory from './ServiceHistory';
import CustomerList from './CustomerList.js';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalespersonHistory from './SalespersonHistory'


function App(props) {
  const [ modelList, setModelList ] = useState([]);
  const [ automobileList, setAutomobileList ] = useState([]);
  const [ automobilesList, setAutomobilesList ] = useState([]);
  const [ manufacturerList, setManufacturerList ] = useState([]);
  const [ appointmentList, setAppointmentList ]  = useState([]);
  const [ technicianList, setTechnicianList ]  = useState([]);
  const [ customerList, setCustomerList ]  = useState([]);
  // eslint-disable-next-line
  const [ automobileVins, setAutomobileVins ] = useState([]);
  const [ showToast, setShowToast ] = useState(false);
  const [ toastMessage, setToastMessage ] = useState('');
  const [ toastVariant, setToastVariant ] = useState('');
  const toast = (
    <>
    <ToastContainer
          className="p-3"
          position={'bottom-end'}
          containerPosition={'absolute'}
          style={{ zIndex: 1 }}
        >
    <Toast onClose={() => setShowToast(false)} show={showToast} delay={4000} autohide animation={true} className={`bg-${toastVariant} text-white align-items-center`}>
      <Toast.Header>
        <strong className={`me-auto ${toastVariant}`}>Notification</strong>
      </Toast.Header>
      <Toast.Body>{toastMessage}</Toast.Body>
    </Toast>
    </ToastContainer>
    </>
  );
  const [ customersList, setCustomersList ] = useState([]);
  const [ salespersonList, setSalespersonList ] = useState([]);
  const [ salesList, setSalesList ] = useState([]);

  async function getAutomobilesList() {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobilesList(data.autos);
    } else {
      console.error(response)
    }
  }

  // Automobile VO for sales form
  async function getAutomobileList() {
    const url = 'http://localhost:8090/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const automobileVins = data.automobiles.map(auto => auto.vin);
      setAutomobileList(data.automobiles);
      setAutomobileVins(automobileVins);
    } else {
      console.error(response)
    }
  }

  async function getSalesList(){
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch (url);
    if (response.ok) {
      const data = await response.json();
      setSalesList(data.sales);
    } else {
      console.error(response);
    }
  }

  async function getSalespersonList() {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch (url);
    if (response.ok) {
      const data = await response.json();
      setSalespersonList(data.salespeople);
    } else {
      console.error(response)
    }
  }

  async function getCustomersList() {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomersList(data.customers);
    } else {
      console.error(response)
    }
  }

  // Customer VO for appointment form
  async function getCustomerList() {
    const url = 'http://localhost:8080/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomerList(data.customers);
    } else {
      console.error(response)
    }
  }

  async function getTechnicianList() {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicianList(data.technicians);
    } else {
      console.error(response)
    }
  }

  async function getAppointmentList() {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointmentList(data.appointments);
      }
    }


  async function getModelList() {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModelList(data.models);
    } else {
      console.error(response)
    }
  }

  async function getManufacturerList() {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      setManufacturerList(data.manufacturers);
    } else {
      console.error(response)
    }
  }


  useEffect(() => {
    getAutomobileList();
    getAutomobilesList();
    getAppointmentList()
    getModelList();
    getManufacturerList();
    getTechnicianList();
    getCustomerList();
    getCustomersList();
    getSalespersonList();
    getSalesList();
  }, []);


  return (
    <BrowserRouter>
      <Nav />
      <main>
      <div className="container-fluid">
        <Routes>
          <Route path="/" element={<MainPage />} />

          {/* Automobiles */}

          <Route path="/automobiles/" element={<AutomobileList getAutomobilesList={getAutomobilesList} automobilesList={automobilesList} setAutomobilesList={setAutomobilesList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} getSalesList={getSalesList} salesList={salesList} setSalesList={setSalesList} />} />

          <Route path="/automobiles/new/" element={<AutomobileForm getAutomobilesList={getAutomobilesList} modelList={modelList} setModelList={setModelList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          {/* Models */}

          <Route path="/models/" element={<ModelList getModelList={getModelList} modelList={modelList} setModelList={setModelList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant}/>} />

          <Route path="/models/new" element={<ModelForm getModelList={getModelList} manufacturerList={manufacturerList} setManufacturerList={setManufacturerList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          {/* Manufacturers */}

          <Route path="/manufacturers/" element={<ManufacturersList getManufacturerList={getManufacturerList} manufacturerList={manufacturerList} setManufacturerList={setManufacturerList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant}/>} />

          <Route path="/manufacturers/new/" element={<ManufacturerForm getManufacturerList={getManufacturerList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          {/* Appointments */}

          <Route path="/appointments/" element={<AppointmentList getAppointmentList={getAppointmentList} appointmentList={appointmentList} setAppointmentList={setAppointmentList}  toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          <Route path="/appointments/history" element={<ServiceHistory getAppointmentList={getAppointmentList} appointmentList={appointmentList} setAppointmentList={setAppointmentList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          <Route path="/appointments/new/" element={<AppointmentForm getAppointmentList={getAppointmentList} getCustomerList={getCustomerList} customerList={customerList} technicianList={technicianList} setTechnicianList={setTechnicianList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant}/>} />

          {/* Technicians */}

          <Route path="/technicians/" element={<TechnicianList getTechnicianList={getTechnicianList} technicianList={technicianList} setTechnicianList={setTechnicianList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          <Route path="/technicians/new/" element={<TechnicianForm getTechnicianList={getTechnicianList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          {/* Customers */}

          <Route path="/customers/" element={<CustomerList getCustomersList={getCustomersList} customersList={customersList}/>} />

          <Route path="/customers/new/" element={<CustomerForm toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} getCustomersList={getCustomersList} />} />

          {/* Salespeople */}

          <Route path="/salespeople/" element={<SalespersonList getSalespersonList={getSalespersonList} salespersonList={salespersonList} setSalespersonList={setSalespersonList} />} />

          <Route path="/salespeople/new/" element={<SalespersonForm getSalespersonList={getSalespersonList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          {/* Sales */}

          <Route path="/sales/" element={<SalesList getSalesList={getSalesList} salesList={salesList} setSalesList={setSalesList} />} />

          <Route path="/sales/new/" element={<SalesForm getSalesList={getSalesList} salesList={salesList}
          automobilesList={automobilesList}getAutomobileList={getAutomobileList} automobileList={automobileList}  getSalespersonList={getSalespersonList} salespersonList={salespersonList} getCustomersList={getCustomersList} customersList={customersList} toast={toast} showToast= {showToast} setShowToast={setShowToast} toastMessage={toastMessage} setToastMessage={setToastMessage} toastVariant={toastVariant} setToastVariant={setToastVariant} />} />

          <Route path="/sales/history/" element={<SalespersonHistory getSalesList={getSalesList} salesList={salesList} getAutomobileList={getAutomobileList} automobileList={automobileList}  getSalespersonList={getSalespersonList} salespersonList={salespersonList} getCustomersList={getCustomerList} customersList={customersList} />} />
        </Routes>
      </div>
      </main>
    </BrowserRouter>
  );
}

export default App;
