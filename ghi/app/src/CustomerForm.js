import React, { useState } from 'react'


function CustomerForm(props) {
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setPhoneNumber] = useState('')

    const handleFirstName = (event) => {
         const value = event.target.value
         setFirstName(value)
    }

    const handleLastName = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleAddress = (event) => {
        const value = event.target.value
        setAddress(value)
    }

    const handlePhoneNUmber = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = first_name
        data.last_name = last_name
        data.address = address
        data.phone_number = phone_number

        const customerUrl = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(customerUrl, fetchConfig)
        if (response.ok) {
            props.getCustomersList()
            props.setShowToast(true);
            props.setToastVariant('success');
            props.setToastMessage(`Customer ${first_name} ${last_name} created.`)
            setFirstName('')
            setLastName('')
            setAddress('')
            setPhoneNumber('')
        } else {
            props.setShowToast(true);
            props.setToastVariant('danger');
            props.setToastMessage("Error creating customer.")
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFirstName} value={first_name} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control"/>
                    <label htmlFor="firstName">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleLastName} value={last_name} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control"/>
                    <label htmlFor="lastName">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleAddress} value={address} placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                    <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePhoneNUmber} value={phone_number} placeholder="Phone Number" required type="text" name="phoneNumber" id="phoneNumber" className="form-control"/>
                    <label htmlFor="phoneNumber">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            {props.toast}
            </div>
        </div>
        );

}

export default CustomerForm
