import React from 'react'

function CustomerList(props) {
    async function deleteCustomer(id) {
        const url = `http://localhost:8090/api/customers/${id}/`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
          props.getCustomersList()
        } else {
          console.error("Error deleting customer")
        }
    }

    let tableRows;
        if (props.customersList.length > 0 ) {
          tableRows = props.customersList.map(customer => {
            return (
              <tr key={customer.id} >
                  <td>{customer.first_name}</td>
                  <td>{customer.last_name}</td>
                  <td>{customer.address}</td>
                  <td>{customer.phone_number}</td>
                  <td>
                  <button type="button" className="btn btn-outline-danger" onClick={() => deleteCustomer(customer.id)}>
                  Delete
                  </button>
                </td>
              </tr>
            )
          })
        } else {
          tableRows =
          <tr>
            <td colSpan="5">No customers created yet.</td>
          </tr>
        }

  return (
    <div>
      <h1>Customer List</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Address</th>
          <th>Phone Number</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {tableRows}
      </tbody>
    </table>
    </div>
  )
}

export default CustomerList;
