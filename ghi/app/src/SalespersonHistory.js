import React, { useState } from 'react'

function SalespersonHistory(props) {


    const [salesperson, setSalesperson] = useState('')

    const handleSalesperson = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }


    const sales = props.salesList.filter((allSales) => allSales.salesperson.id == salesperson)

    let tableRows = sales.map((sale) => (
      <tr key={sale.id}>
        <td> {sale.salesperson.first_name} {sale.salesperson.last_name} </td>
        <td> {sale.customer.first_name} {sale.customer.last_name} </td>
        <td>{sale.automobile.vin}</td>
        <td>{sale.price}</td>
      </tr>
    ))

  if (tableRows.length === 0) {
    tableRows = (
      <tr>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
        <td>N/A</td>
      </tr>
    )
  }



    return (

        <div className="mb-3">
        <h1>Salesperson History</h1>
        <select onChange={handleSalesperson} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
            <option>Choose a salesperson...</option>
            {props.salespersonList.map( salesperson => {
                return (
                    <option key={salesperson.id} value={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name} | Employee ID: {salesperson.employee_id}
                    </option>
                    );
                })}
        </select>


        <table className="table table-striped">
        <thead>
        <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            {/* <th>Delete</th> */}
        </tr>
        </thead>
        <tbody>
                {tableRows}
        </tbody>
    </table>
    </div>
    )

    }

    export default SalespersonHistory
