import React from 'react'

function SalesList(props) {
    async function deleteSale(id) {
        const url = `http://localhost:8090/api/sales/${id}/`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
          props.getSalesList()
        } else {
          console.error("Error deleting customer")
        }
    }

    let tableRows;
        if (props.salesList.length > 0 ) {


          tableRows = props.salesList.map( sale => {
            return(
            <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name} </td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
                <td>
                <button type="button" className="btn btn-outline-danger" onClick={() => deleteSale(sale.id)}>
                Delete
                </button>
                </td>
            </tr>
            )
          })}
         else {
          tableRows =
          <tr>
            <td colSpan="8">No sales recorded yet.</td>
          </tr>
        }

    return (
        <div>
          <h1>Sales</h1>
        <table className="table table-striped">
        <thead>
        <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        {tableRows}
        </tbody>
      </table>
      </div>
    )
}

export default SalesList
