from django.db import models
from django.urls import reverse

class Status(models.Model):
    """
    The Status model provides a status to an appointment, which
    can be SCHEDULED, FINISHED, or CANCELED.

    Status is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "statuses"  # Fix the pluralization

class AutomobileVO(models.Model):
    """
    AutomobileVO is a Value Object and, therefore, does not have a
    direct URL to view it.
    """
    automobile_vo_id = models.PositiveSmallIntegerField(unique=True)
    vin = models.CharField(max_length=30)
    sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_automobile_vo", kwargs={"vin": self.vin, "sold": self.sold})

    def __str__(self):
        return f"{self.vin} / sold: {self.sold}"

    class Meta:
        ordering = ("vin",)
        verbose_name = "AutomobileVO"
        verbose_name_plural = "AutomobileVOs"


class Technician(models.Model):
    """
    Technician model is associated with service appointments.
    """
    employee_id = models.CharField(max_length=100, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_technician", kwargs={
            "employee_id": self.employee_id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            }
        )

    def __str__(self):
        return f"{self.first_name}  {self.last_name}"

    class Meta:
        ordering = ("last_name", "first_name")
        verbose_name = "Technician"
        verbose_name_plural = "Technicians"


class CustomerVO(models.Model):
    """
    CustomerVO is a Value Object and, therefore, does not have a
    direct URL to view it.
    """
    customer_vo_id = models.PositiveSmallIntegerField()
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=12)

    def __str__(self):
        return f"{self.first_name}  {self.last_name}"

    class Meta:
        ordering = ("last_name", "first_name")
        verbose_name = "CustomerVO"
        verbose_name_plural = "CustomerVOs"


class Appointment(models.Model):
    """
    Appointment model represents service appointments.
    """
    @classmethod
    def create(cls, **kwargs):
        """
        This function defaults new appointments to a status of "SCHEDULED".
        """
        kwargs["status"] = Status.objects.get(name="SCHEDULED")
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=300)
    customer = models.ForeignKey(
        CustomerVO,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    status = models.ForeignKey(
        Status,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    vin = models.CharField(max_length=30)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    def finish(self):
        """
        This function sets an appointments to a status of "FINISHED".
        """
        status = Status.objects.get(name="FINISHED")
        self.status = status
        self.save()

    def cancel(self):
        """
        This function sets an appointments to a status of "CANCELED".
        """
        status = Status.objects.get(name="CANCELED")
        self.status = status
        self.save()

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={
            "pk": self.pk,
        })

    def __str__(self):
        return f"{self.vin} / {self.date_time}"

    class Meta:
        ordering = ("date_time", "technician")
        verbose_name = "Appointment"
        verbose_name_plural = "Appointments"
